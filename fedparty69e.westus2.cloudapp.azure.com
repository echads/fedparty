server {
    listen 443 ssl http2;
    listen [::]:443 ssl http2;

    server_name fedparty69e.westus2.cloudapp.azure.com;

    ssl_certificate /etc/letsencrypt/live/fedparty69e.westus2.cloudapp.azure.com/fullchain.pem;
    ssl_certificate_key /etc/letsencrypt/live/fedparty69e.westus2.cloudapp.azure.com/privkey.pem;

    access_log /home/desktop/fedparty69e.westus2.cloudapp.azure.com/logs/access.log;
    error_log /home/desktop/fedparty69e.westus2.cloudapp.azure.com/logs/error.log;

    root /var/www/html;

    index info.php index.php index.html index.htm index.nginx-debian.html;

    location / {
        try_files $uri $uri/ /index.php?$args;
    }

    location ~ \.php$ {
        try_files $uri =404;
        fastcgi_split_path_info ^(.+\.php)(/.+)$;
        fastcgi_pass unix:/run/php/php7.4-fpm.sock;
        fastcgi_index index.php;
        include fastcgi_params;
    }
}

server {
    listen 443 ssl http2;
    listen [::]:443 ssl http2;

    server_name www.fedparty69e.westus2.cloudapp.azure.com;

    ssl_certificate /etc/letsencrypt/live/fedparty69e.westus2.cloudapp.azure.com/fullchain.pem;
    ssl_certificate_key /etc/letsencrypt/live/fedparty69e.westus2.cloudapp.azure.com/privkey.pem;

    return 301 https://fedparty69e.westus2.cloudapp.azure.com$request_uri;
}

server {
    listen 80;
    listen [::]:80;

    server_name fedparty69e.westus2.cloudapp.azure.com www.fedparty69e.westus2.cloudapp.azure.com;

    return 301 https://fedparty69e.westus2.cloudapp.azure.com$request_uri;
}

